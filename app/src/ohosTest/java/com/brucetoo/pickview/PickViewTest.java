/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.brucetoo.pickview;

import com.bruce.pickerview.utils.PickUtils;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * PickView单元测试
 */
public class PickViewTest extends TestCase {
    /**
     * getColorFromRes方法测试用例
     */
    @Test
    public void testGetColorFromRes() {
        Context appContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Color color = PickUtils.getColorFromRes(appContext, ResourceTable.Color_purple_700);
        Assert.assertNotNull(color);
    }

    /**
     * vpToPx方法测试用例类
     */
    @Test
    public void testVpToPx() {
        Context appContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int px = PickUtils.vpToPx(appContext, 10);
        assertThat(px, is(30));
    }

    /**
     * format2LenStr方法测试用例类
     */
    @Test
    public void testFormat2LenStr() {
        String date = PickUtils.format2LenStr(4);
        assertThat(date, is("04"));
    }

    /**
     * getLongFromyyyyMMdd方法测试用例类
     */
    @Test
    public void testGetLongFromyyyyMMdd() {
        long date = PickUtils.getLongFromyyyyMMdd("2013-11-11");
        assertThat(date, is(1384099200000L));
    }

}