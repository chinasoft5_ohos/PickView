package com.brucetoo.pickview;

import com.bruce.pickerview.LoopView;
import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.bruce.pickerview.popwindow.TimePickerPopWin;
import com.bruce.pickerview.utils.PickUtils;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

public class MainAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        process();
        findComponentById(ResourceTable.Id_btn_date).setClickedListener(cpt -> {
            DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(this, (year, month, day, dateDesc) ->
                    new ToastDialog(getApplicationContext())
                            .setText(dateDesc)
                            .show()
            ).textConfirm("CONFIRM")
                    .textCancel("CANCEL")
                    .btnTextSize(16)
                    .colorCancel(Color.getIntColor("#999999"))
                    .colorConfirm(Color.getIntColor("#009900"))
                    .minYear(1990)
                    .maxYear(2550)
                    .dateChose("2013-11-11")
                    .build();
            pickerPopWin.showPopWin();
        });
        findComponentById(ResourceTable.Id_btn_time).setClickedListener(cpt -> {
            TimePickerPopWin timePickerPopWin = new TimePickerPopWin.Builder(this, (hour, minute, ap, time) ->
                    new ToastDialog(getApplicationContext())
                            .setText(time)
                            .show()
            ).textConfirm("CONFIRM")
                    .textCancel("CANCEL")
                    .btnTextSize(16)
                    .colorCancel(Color.getIntColor("#999999"))
                    .colorConfirm(Color.getIntColor("#009900"))
                    .build();
            timePickerPopWin.showPopWin();
        });
        findComponentById(ResourceTable.Id_btn_province).setClickedListener(cpt ->
                new ToastDialog(getApplicationContext())
                        .setText("Working on...")
                        .show()
        );
        LoopView loopView = (LoopView) findComponentById(ResourceTable.Id_loop_view);
        loopView.setInitPosition(2);
        loopView.setCanLoop(false);
        loopView.setLoopListener(item -> {
        });
        loopView.setTextSize(25);
        loopView.setDataList(getList());
    }

    private ArrayList<String> getList() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add("DAY TEST:" + i);
        }
        return list;
    }

    private void process() {
        Color color = PickUtils.getColorFromRes(this, ResourceTable.Color_purple_700);
        getWindow().setStatusBarColor(color.getValue());
        initFloatAction(findComponentById(ResourceTable.Id_dl_flat));
    }

    /**
     * 初始化FloatActionButton
     *
     * @param cpt 组件Cpt
     */
    private void initFloatAction(Component cpt) {
        try {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setCornerRadius(cpt.getHeight() * 2);
            shapeElement.setRgbColor(RgbColor.fromArgbInt(
                    getResourceManager().getElement(
                            ResourceTable.Color_bgSnackFlat).getColor()));
            cpt.setBackground(shapeElement);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

}