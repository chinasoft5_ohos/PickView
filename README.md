# PickView

#### 项目介绍
- 项目名称：PickView
- 所属系列：openharmony的第三方组件适配移植
- 功能：PickView是一个展示日期及时间滚轮显示效果的功能库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：release 1.2.3

#### 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/095216_70385eb8_952271.gif "PickView.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

             url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

    implementation('com.gitee.chinasoft_ohos:PickView:1.0.0')

    ......  

 }

 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

完整使用:

```java

<com.bruce.pickerview.LoopView
                ohos:id="$+id:loop_view"
                ohos:height="140vp"
                ohos:width="200vp"
                ohos:left_margin="15vp"
                ohos:top_margin="20vp"
                hap:canLoop="true"
                hap:centerTextColor="#ff000000"
                hap:lineColor="$color:colorPrimary"
                hap:textSize="25"
                hap:topBottomTextColor="#ffafafaf"/>

LoopView loopView = (LoopView) findComponentById(ResourceTable.Id_loop_view);
        loopView.setInitPosition(2);
        loopView.setCanLoop(false);
        loopView.setLoopListener(item -> {
        });
        loopView.setTextSize(25);
        loopView.setDataList(getList());

```

关于 [DatePicker]

```java

findComponentById(ResourceTable.Id_btn_date).setClickedListener(cpt -> {
            DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(this, (year, month, day, dateDesc) ->
                    new ToastDialog(getApplicationContext())
                            .setText(dateDesc)
                            .show()
            ).textConfirm("CONFIRM")
                    .textCancel("CANCEL")
                    .btnTextSize(16)
                    .colorCancel(Color.getIntColor("#999999"))
                    .colorConfirm(Color.getIntColor("#009900"))
                    .minYear(1990)
                    .maxYear(2550)
                    .dateChose("2013-11-11")
                    .build();
            pickerPopWin.showPopWin();
        });

```

关于 [TimePicker]

```java

findComponentById(ResourceTable.Id_btn_time).setClickedListener(cpt -> {
            TimePickerPopWin timePickerPopWin = new TimePickerPopWin.Builder(this, (hour, minute, ap, time) ->
                    new ToastDialog(getApplicationContext())
                            .setText(time)
                            .show()
            ).textConfirm("CONFIRM")
                    .textCancel("CANCEL")
                    .btnTextSize(16)
                    .colorCancel(Color.getIntColor("#999999"))
                    .colorConfirm(Color.getIntColor("#009900"))
                    .build();
            timePickerPopWin.showPopWin();
        });

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息

Copyright 2015 - 2016 Bruce too

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

See [LICENSE](LICENSE) file for details.
