/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bruce.pickerview.popwindow;

import com.bruce.pickerview.LoopView;
import com.bruce.pickerview.ResourceTable;
import com.bruce.pickerview.utils.PickUtils;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.PopupDialog;

import ohos.app.Context;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 时间滚轮工具类
 */
public class TimePickerPopWin extends PopupDialog implements Component.ClickedListener {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x000110, "DatePickerPopWin----");
    private Component rootView;
    private Component pickView;
    private Component outerView;
    private Button cancel;
    private Button confirm;
    private LoopView pickHour;
    private LoopView pickMinute;
    private LoopView pickMeridian;

    private int hourPos = 0;
    private int minutePos = 0;
    private int meridianPos = 0;
    private String textCancel;
    private String textConfirm;
    private int colorCancel;
    private int colorConfirm;
    private int btnTextsize;
    private OnTimePickListener mListener;

    private final List<String> hourList = new ArrayList<>();
    private final List<String> minList = new ArrayList<>();
    private final List<String> meridianList = new ArrayList<>();

    private AnimatorProperty pickAnim;

    private TimePickerPopWin(Context context, Component contentComponent, Builder builder) {
        super(context, contentComponent);
        init(context, builder);
    }

    private TimePickerPopWin(Context context, Component contentComponent, int width, int height, Builder builder) {
        super(context, contentComponent, width, height);
        init(context, builder);
    }

    private void init(Context context, Builder builder) {
        this.textCancel = builder.textCancel;
        this.textConfirm = builder.textConfirm;
        this.mListener = builder.listener;
        this.colorCancel = builder.colorCancel;
        this.colorConfirm = builder.colorConfirm;
        this.btnTextsize = builder.btnTextSize;
        initView(context);
    }

    private void initView(Context context) {
        rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_time_picker, null, true);
        pickView = rootView.findComponentById(ResourceTable.Id_container_picker);
        outerView = rootView.findComponentById(ResourceTable.Id_cpt_outer);
        cancel = (Button) rootView.findComponentById(ResourceTable.Id_btn_cancel);
        confirm = (Button) rootView.findComponentById(ResourceTable.Id_btn_confirm);
        pickHour = (LoopView) rootView.findComponentById(ResourceTable.Id_picker_hour);
        pickMinute = (LoopView) rootView.findComponentById(ResourceTable.Id_picker_minute);
        pickMeridian = (LoopView) rootView.findComponentById(ResourceTable.Id_picker_meridian);
        outerView.setClickedListener(this);
        cancel.setClickedListener(this);
        confirm.setClickedListener(this);
        pickHour.setLoopListener(item -> hourPos = item);
        pickMinute.setLoopListener(item -> minutePos = item);
        pickMeridian.setLoopListener(item -> meridianPos = item);
        initPickerViews();
        cancel.setTextColor(new Color(colorCancel));
        confirm.setTextColor(new Color(colorConfirm));
        cancel.setTextSize(AttrHelper.fp2px(btnTextsize, context));
        confirm.setTextSize(AttrHelper.fp2px(btnTextsize, context));
        if (textCancel != null && !"".equals(textCancel)) {
            cancel.setText(textCancel);
        }
        if (textConfirm != null && !"".equals(textConfirm)) {
            confirm.setText(textConfirm);
        }
        setCustomComponent(rootView);
        setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        setTransparent(true);
        setBackColor(new Color(Color.getIntColor("#33000000")));
    }

    /**
     * 点击事件处理
     *
     * @param cpt cpt
     */
    @Override
    public void onClick(Component cpt) {
        if (cpt == outerView || cpt == cancel) {
            pickAnim(false);
        } else if (cpt == confirm) {
            if (mListener != null) {
                String amPm = meridianList.get(meridianPos);
                String sb = hourList.get(hourPos) +
                        ":" +
                        minList.get(minutePos) +
                        amPm;
                mListener.onTimePickCompleted(hourPos + 1, minutePos, amPm, sb);
            }
            pickAnim(false);
        } else {
            HiLog.info(TAG, "");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pickAnim != null) {
            pickAnim = null;
        }
    }

    private void initPickerViews() {

        hourPos = Calendar.getInstance().get(Calendar.HOUR) - 1;
        minutePos = Calendar.getInstance().get(Calendar.MINUTE);
        meridianPos = Calendar.getInstance().get(Calendar.AM_PM);

        for (int i = 1; i <= 12; i++) {
            hourList.add(PickUtils.format2LenStr(i));
        }

        for (int j = 0; j < 60; j++) {
            minList.add(PickUtils.format2LenStr(j));
        }

        meridianList.add("AM");
        meridianList.add("PM");

        pickHour.setDataList(hourList);
        pickHour.setInitPosition(hourPos);

        pickMinute.setDataList(minList);
        pickMinute.setInitPosition(minutePos);

        pickMeridian.setDataList(meridianList);
        pickMeridian.setInitPosition(meridianPos);
    }

    private void pickAnim(boolean flag) {
        if (pickAnim == null) {
            pickAnim = pickView.createAnimatorProperty();
        }
        pickAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
                dismiss(flag);
            }

            @Override
            public void onCancel(Animator animator) {
                dismiss(flag);
            }

            @Override
            public void onEnd(Animator animator) {
                dismiss(flag);
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        int rh = rootView.getHeight();
        int ph = pickView.getHeight();
        float start = flag ? rh : rh - ph;
        float stop = flag ? rh - ph : rh;
        pickAnim.moveFromY(start).moveToY(stop).setDuration(400).start();
    }

    private void dismiss(boolean flag) {
        if (!flag) {
            hide();
        }
    }

    /**
     * 时间选择器显示
     */
    public void showPopWin() {
        show();
        pickAnim(true);
    }

    /**
     * 时间选择器构造类
     */
    public static class Builder {
        private final Context context;
        private final OnTimePickListener listener;
        private String textCancel = "Cancel";
        private String textConfirm = "Confirm";
        private int colorCancel = Color.getIntColor("#999999");
        private int colorConfirm = Color.getIntColor("#303F9F");
        private int btnTextSize = 16;

        /**
         * 构造函数
         *
         * @param context  context
         * @param listener listener
         */
        public Builder(Context context, OnTimePickListener listener) {
            this.context = context;
            this.listener = listener;
        }

        /**
         * 取消按钮文本
         *
         * @param textCancel 文本
         * @return builder
         */
        public Builder textCancel(String textCancel) {
            this.textCancel = textCancel;
            return this;
        }

        /**
         * 确认按钮文本
         *
         * @param textConfirm 文本
         * @return builder
         */
        public Builder textConfirm(String textConfirm) {
            this.textConfirm = textConfirm;
            return this;
        }

        /**
         * 取消按钮颜色
         *
         * @param colorCancel 颜色
         * @return builder
         */
        public Builder colorCancel(int colorCancel) {
            this.colorCancel = colorCancel;
            return this;
        }

        /**
         * 确认按钮颜色
         *
         * @param colorConfirm 颜色
         * @return builder
         */
        public Builder colorConfirm(int colorConfirm) {
            this.colorConfirm = colorConfirm;
            return this;
        }

        /**
         * 文本大小
         *
         * @param textSize 大小
         * @return builder
         */
        public Builder btnTextSize(int textSize) {
            this.btnTextSize = textSize;
            return this;
        }

        /**
         * 构造
         *
         * @return TimePick时间选择器
         */
        public TimePickerPopWin build() {
            return new TimePickerPopWin(context, null, this);
        }

    }

    /**
     * 时间选择监听器
     */
    public interface OnTimePickListener {

        void onTimePickCompleted(int hour, int minute, String ap, String time);

    }

}