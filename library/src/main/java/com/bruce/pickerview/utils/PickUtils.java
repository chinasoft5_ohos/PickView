/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bruce.pickerview.utils;

import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;

/**
 * PickView工具类
 */
public class PickUtils {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x000110, "PickUtils----");

    /**
     * 获取今天日期字符串
     *
     * @return 日期
     */
    public static String getStrDate() {
        SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return dd.format(new Date());
    }

    /**
     * 获取指定日期毫秒值
     *
     * @param date 日期
     * @return 毫秒值
     */
    public static long getLongFromyyyyMMdd(String date) {
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date parse = null;
        try {
            parse = mFormat.parse(date);
        } catch (ParseException e) {
            HiLog.info(TAG, "error : " + e.getMessage());
        }
        if (parse != null) {
            return parse.getTime();
        } else {
            return -1;
        }
    }

    /**
     * 格式化日期字符串
     *
     * @param num 日期
     * @return 日期
     */
    public static String format2LenStr(int num) {
        return (num < 10) ? "0" + num : String.valueOf(num);
    }

    /**
     * vp转px
     *
     * @param context 上下文
     * @param vp      vp
     * @return px
     */
    public static int vpToPx(Context context, int vp) {
        DisplayAttributes da = DisplayManager.getInstance()
                .getDefaultDisplay(context).get().getAttributes();
        return Math.round(da.densityPixels * vp);
    }

    /**
     * 从color.json中获取Color对象
     *
     * @param context 上下文
     * @param res     color.json中的color
     * @return Color对象
     */
    public static Color getColorFromRes(Context context, int res) {
        Color color = null;
        try {
            color = new Color(context.getResourceManager().getElement(res).getColor());
        } catch (IOException | NotExistException | WrongTypeException e) {
            HiLog.info(TAG, "error : " + e.getMessage());
        }
        return color;
    }
}