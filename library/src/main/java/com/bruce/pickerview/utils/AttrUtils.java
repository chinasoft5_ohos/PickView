/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bruce.pickerview.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.VectorElement;

import ohos.media.image.PixelMap;

/**
 * Attr属性工具类
 */
public final class AttrUtils {
    /**
     * 获取Attr属性集
     *
     * @param attrSet Attr集合
     * @param name    Attr名称
     * @return Attr
     */
    private static Attr getAttr(AttrSet attrSet, String name) {
        Attr attr = null;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            attr = attrSet.getAttr(name).get();
        }
        return attr;
    }

    /**
     * 获取Attr属性指定Integer值
     *
     * @param attrSet      AttrSet
     * @param name         名称
     * @param defaultValue 默认值
     * @return 返回值
     */
    public static int getInteger(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            value = attr.getIntegerValue();
        }
        return value;
    }

    /**
     * 获取Attr属性指定Float值
     *
     * @param attrSet      AttrSet
     * @param name         名称
     * @param defaultValue 默认值
     * @return 返回值
     */
    public static float getFloat(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            value = attr.getFloatValue();
        }
        return value;
    }

    /**
     * 获取Attr属性指定Long值
     *
     * @param attrSet      AttrSet
     * @param name         名称
     * @param defaultValue 默认值
     * @return 返回值
     */
    public static Long getLong(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            value = attr.getLongValue();
        }
        return value;
    }

    /**
     * 获取Attr属性指定Boolean值
     *
     * @param attrSet      AttrSet
     * @param name         名称
     * @param defaultValue 默认值
     * @return 返回值
     */
    public static boolean getBoolean(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            value = attr.getBoolValue();
        }
        return value;
    }

    /**
     * 获取Attr属性指定String值
     *
     * @param attrSet      AttrSet
     * @param name         名称
     * @param defaultValue 默认值
     * @return 返回值
     */
    public static String getString(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            value = attr.getStringValue();
        }
        return value;
    }

    /**
     * 获取Attr属性指定Color值
     *
     * @param attrSet      AttrSet
     * @param name         名称
     * @param defaultValue 默认值
     * @return 返回值
     */
    public static int getColor(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            value = attr.getColorValue().getValue();
        }
        return value;
    }

    /**
     * 获取Attr属性指定PixelMap值
     *
     * @param attrSet AttrSet
     * @param name    名称
     * @return 返回值
     */
    public static PixelMap getPixelMap(AttrSet attrSet, String name) {
        PixelMap value = null;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            Element element = attr.getElement();
            if (element instanceof PixelMapElement) {
                value = ((PixelMapElement) element).getPixelMap();
            }
        }
        return value;
    }

    /**
     * 获取Attr属性指定Vector值
     *
     * @param attrSet AttrSet
     * @param name    名称
     * @return 返回值
     */
    public static VectorElement getVector(AttrSet attrSet, String name) {
        VectorElement value = null;
        Attr attr = getAttr(attrSet, name);
        if (attr != null) {
            Element element = attr.getElement();
            if (element instanceof VectorElement) {
                value = (VectorElement) element;
            }
        }
        return value;
    }
}